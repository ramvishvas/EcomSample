package com.allandroidprojects.ecomsample.cooperative;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.allandroidprojects.ecomsample.R;
import com.allandroidprojects.ecomsample.startup.MainActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class PaymentActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;

    private EditText name, email, mobile, address, city, pin, state;
    private TextView vname, vemail, vmobile, vaddress, vcity, vpin, vstate, amount;
    private Button nextBtn, printBtn;
    private ScrollView detail;
    private LinearLayout linearLayout;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        initView();
        amount.setText("Total Amount Paid : "+ getIntent().getStringExtra("money"));
        mAuth = FirebaseAuth.getInstance();
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lname = name.getText().toString().trim();
                String lemail = email.getText().toString().trim();
                String lmobile = mobile.getText().toString().trim();
                String laddress = address.getText().toString().trim();
                String lcity = city.getText().toString().trim();
                String lpin = pin.getText().toString().trim();
                String lstate = state.getText().toString().trim();
                if (!isFieldEmpty(lname, lemail, lmobile,laddress, lcity, lpin, lstate)){
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    setTextMethod(lname, lemail, lmobile,laddress, lcity, lpin, lstate);
                    detail.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void setTextMethod(String lname, String lemail, String lmobile, String laddress, String lcity, String lpin, String lstate) {
        vname.setText(lname);
        vemail.setText(lemail);
        vmobile.setText(lmobile);
        vaddress.setText(laddress);
        vcity.setText(lcity);
        vpin.setText(lpin);
        vstate.setText(lstate);
    }

    private boolean isFieldEmpty(String name, String email, String mobile, String address, String city, String pin, String state) {
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(PaymentActivity.this, "Provide Full Name", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(email)) {
            Toast.makeText(PaymentActivity.this, "Provide Email", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(mobile)) {
            Toast.makeText(PaymentActivity.this, "Provide Mobile", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(address)) {
            Toast.makeText(PaymentActivity.this, "Provide Address", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(city)) {
            Toast.makeText(PaymentActivity.this, "Provide City", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(pin)) {
            Toast.makeText(PaymentActivity.this, "Provide Pin Code", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(state)) {
            Toast.makeText(PaymentActivity.this, "Provide State", Toast.LENGTH_SHORT).show();
        } else {
            return false;
        }
        return true;
    }

    private void initView() {
        name =  findViewById(R.id.name);
        email = findViewById(R.id.email);
        mobile = findViewById(R.id.mobile);
        address = findViewById(R.id.address);
        city = findViewById(R.id.city);
        pin = findViewById(R.id.pin);
        state = findViewById(R.id.state);

        vname =  findViewById(R.id.vname);
        vemail = findViewById(R.id.vemail);
        vmobile = findViewById(R.id.vmobile);
        vaddress = findViewById(R.id.vaddress);
        vcity = findViewById(R.id.vcity);
        vpin = findViewById(R.id.vpin);
        vstate = findViewById(R.id.vstate);
        amount = findViewById(R.id.total_amount);

        nextBtn = findViewById(R.id.btn_next);
        printBtn = findViewById(R.id.btn_print);

        detail = findViewById(R.id.detail);
        linearLayout = findViewById(R.id.linear);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.account_menu, menu);
        return true;
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            sendToLoginActivity();
        }
    }

    private void sendToLoginActivity() {
        startActivity(new Intent(PaymentActivity.this, LoginActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.account_logout:
                logOut();
                return true;

            case R.id.account_setting:
                //sendToSetup();
                return true;

            default:
                return false;
        }
    }

    private void logOut() {
        mAuth.signOut();
        startActivity(new Intent(PaymentActivity.this, MainActivity.class));
        finish();
    }
}
